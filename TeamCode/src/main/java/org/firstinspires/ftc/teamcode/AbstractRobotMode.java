package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.AnalogInput;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;

//
public abstract class AbstractRobotMode extends OpMode {

    protected Gamepad gamepadA, gamepadB = null;

    //imu = expansion hub
    protected BNO055IMU imu;

    @Override
    public void init() {
        gamepadA = gamepad1; // renaming the gamepad to match
        gamepadB = gamepad2;

        // Gyro
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.mode                = BNO055IMU.SensorMode.IMU;
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.loggingEnabled      = false;
        imu = hardwareMap.get(BNO055IMU.class, "imu");
        imu.initialize(parameters);
    }


    protected double angleZ = .0; // the one we care
    protected double angleY = .0;
    protected double angleX = .0;


    protected void readAngles() {

        Orientation angles = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        angleZ = AngleUnit.DEGREES.fromUnit(angles.angleUnit, angles.firstAngle);
        angleY = AngleUnit.DEGREES.fromUnit(angles.angleUnit, angles.secondAngle);
        angleX = AngleUnit.DEGREES.fromUnit(angles.angleUnit, angles.thirdAngle);

    }

}
