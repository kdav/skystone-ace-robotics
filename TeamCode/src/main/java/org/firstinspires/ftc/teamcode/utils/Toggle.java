package org.firstinspires.ftc.teamcode.utils;

import com.qualcomm.robotcore.hardware.Gamepad;

public class Toggle {

    Gamepad gamepad;
    boolean b_released = true, b_on  = false,
            x_released = true, x_on = false;


    public Toggle(Gamepad gamepad){
        this.gamepad = gamepad;
    }

    public boolean isB()
    {
        if(gamepad.b && b_released)
        {
            b_released = false;
            b_on = !b_on;
        }
        else if(!gamepad.b)
        {
            b_released = true;
        }
        return b_on;
    }


    public boolean isX()
    {
        if(gamepad.x && x_released)
        {
            x_released = true;
            x_on = !x_on;
        }
        else if(!gamepad.x)
        {
            x_released = true;
        }
        return x_on;
    }

}
