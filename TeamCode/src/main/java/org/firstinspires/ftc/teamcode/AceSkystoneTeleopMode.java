package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.subsystems.DriveTrain;
import org.firstinspires.ftc.teamcode.utils.Toggle;

@TeleOp
public class AceSkystoneTeleopMode extends AbstractRobotMode {

    double referenceAngle = .0;

    Toggle toggles;
    DriveTrain driveTrain;
    @Override
    public void init() {
        super.init();

        driveTrain = new DriveTrain(hardwareMap);
        toggles = new Toggle(gamepadA);
    }

    @Override
    public void loop() {

        driveTrain.GamepadMove(gamepadA);

        double left_stick_y  = gamepadA.left_stick_y;
        double right_stick_x = gamepadA.right_stick_x;

        //telemetry.addData("Elevator Speed", "(%.2f)", intakeElevatorSpeed);

        // Gyro
        readAngles();
        telemetry.addData("Angles", "Ref(%.1f) Z(%.1f)", referenceAngle, angleZ);
//        telemetry.addData("Angles", "Z(%.1f), Y(%.1f), X(%.1f)", angleZ, angleY, angleX);


        // Leave telemetry update at the end
        telemetry.update();
    }
}
