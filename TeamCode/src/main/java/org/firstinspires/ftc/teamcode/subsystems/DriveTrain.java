package org.firstinspires.ftc.teamcode.subsystems;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.HardwareMap;
//test
public class DriveTrain {
    protected DcMotor leftTopMotor;
    protected DcMotor leftBottomMotor;
    protected DcMotor rightTopMotor;
    protected DcMotor rightBottomMotor;

    public DriveTrain(HardwareMap hardwareMap) {
        leftTopMotor = hardwareMap.get(DcMotor.class, "motor_left_top");
        leftBottomMotor = hardwareMap.get(DcMotor.class, "motor_left_bottom");
        rightTopMotor = hardwareMap.get(DcMotor.class, "motor_right_top");
        rightBottomMotor = hardwareMap.get(DcMotor.class, "motor_left_bottom");
    }

    public void GamepadMove(Gamepad gamepad1) {
        double r = Math.hypot(gamepad1.left_stick_x, gamepad1.left_stick_y);
        double robotAngle = Math.atan2(gamepad1.left_stick_y, gamepad1.left_stick_x) - Math.PI / 4;
        double rightX = gamepad1.right_stick_x;
        final double v1 = r * Math.cos(robotAngle) + rightX;
        final double v2 = r * Math.sin(robotAngle) - rightX;
        final double v3 = r * Math.sin(robotAngle) + rightX;
        final double v4 = r * Math.cos(robotAngle) - rightX;

        leftTopMotor.setPower(v1);
        rightTopMotor.setPower(v2);
        leftBottomMotor.setPower(v3);
        rightBottomMotor.setPower(v4);
    }
}
